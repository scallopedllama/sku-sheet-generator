# SKU Sheet Generator

AGPL3.0 licensed client-side JS static page for generating SKU label sheets.

## Setup

Before attempting to print SKU label sheets, your browser must be configured properly:

1. Pull up the print preview for the page
2. Ensure that the orientation is Portrait
3. Set the scale to 100%

If you do not make sure that the scale is 100%, the generated labels will not fit
properly on the label sheet when printed.

## Usage

Load up `index.html` and load in your SKUs. The description field is optional.

Once all your SKUs are loaded, click "Print SKU Label Sheets" to generate label
sheets containing as many labels per SKU as requested. Print the generated labels
directly to an Avery 5160 template sheet.

Additionally, The SKU Sample Sheet will provide one larger, easier to read label per SKU.
The Sample Sheet is useful for having a sheet of all your SKUs to scan from.
