$( document ).ready(function() {
	
	// Remove SKU button functionality
	$('#sku-entry-empty .remove').click(function(event) {
		$(this).parent().parent().remove();
	});
	
	// Hide SKU button functionality
	$('#sku-entry-empty .hide').click(function(event) {
		// Get sku and item values
		var parent = $(this).parent();
		
		if (!CheckRequiredEntries(parent))
		{
			return false;
		}
		
		// Hide the hide button
		$(this).hide();
		// Fill the header accordingly and show it
		parent.find('.header-sku').html(parent.find('.sku').val());
		parent.find('.header-item').html(parent.find('.item').val());
		parent.find('.header-price').html('$ ' + parent.find('.price').val());
		parent.children('.sku-entry-header').slideDown();
		// Hide the body contents
		parent.children('.sku-entry-body').slideUp();
		// Show the show button
		parent.children('.show').show();
	});
	
	// Show SKU button functionality
	$('#sku-entry-empty .show').click(function(event) {
		var parent = $(this).parent();
		$(this).hide();
		parent.children('.sku-entry-header').slideUp();
		parent.children('.sku-entry-body').slideDown();
		parent.children('.hide').show();
	});
	
	// Add SKU button functionality
	$('#add-sku-entry').click(function(event) {
		AddSkuEntry();
	});
	
	// Automatic formatting for price field
	$('#sku-entry-empty .price').change(function(event) {
		var re = /^(\d+)(\.\d{2})?$/
		var capture = re.exec($(this).val());
		if (capture)
		{
			var dollar = capture[1];
			var cents = capture[2] ? capture[2] : ".00";
			$(this).val('$' + dollar + cents);
		}
		else
		{
			$(this).val('');
		}
	});
	
	
	// Print SKU Label Sheets
	$('#print-labels').click(function(event) {
		if (SkusAdded())
		{
			$('#print-label-dialog').show();
			$('#overlay').fadeIn();
		}
	});
	$('#print-labels-print').click(function(event) {
		SetStylesheet('avery-5160.css');
		Print($('#print-labels-count').val());
	});
	
	
	// Print SKU Sample Sheet
	$('#print-sample').click(function(event) {
		SetStylesheet('sku-sample-sheet.css');
		Print(1);
	});
	
	
	
	// Save button
	$('#save').click(function(event) {
		if (SkusAdded())
		{
			Save();
		}
	});
	
	// Load button shows overlay
	$('#load').click(function(event) {
		// Ask for confirmation if they have any sku entries added
		if ($('.sku-entry').length && ! confirm("You have potentially unsaved changes.\nAre you sure you want to load?") )
		{
			return false;
		}
		// Clear the file picker
		$('#load-file').val('');
		// Show the load dialog
		$('#load-dialog').show();
		// Fade in the load overlay
		$('#overlay').fadeIn();
	});
	// Cancel hides the load overlay
	$('.overlay-cancel').click(HideOverlay);
	// Whenever the load file picker changes, load up the data
	$('#load-file').change(function() {
		var reader = new FileReader();
		reader.onload = function(event) {
			var data = event.target.result;
			if (! LoadData(data) )
			{
				alert("Failed to load CSV file. You probably picked the wrong file.");
			}
		};
		reader.onerror = function(event) {
			alert("Failed to load CSV file. You probably picked the wrong file.");
		};
		reader.readAsText(this.files[0]);
	});
	
	$('#new').click(function(event) {
		if ($('.sku-entry').length && confirm("You have potentially unsaved changes.\nAre you sure you want to start over?") )
		{
			Clear();
		}
	});
	
	// Add leave confirmation
	window.onbeforeunload = function() {
		return "Make sure you save!";
	};
});

function SetStylesheet(sheet)
{
	$('#label-style').attr({href: sheet});
}

function HideOverlay()
{
	$('#overlay').fadeOut(function() {
		$('.dialog').hide();
	});
}

// Adds a new SKU entry and returns the jQuery object for it
function AddSkuEntry()
{
	// Need to do a deep clone of events to get show / hide and remove handlers
	return $('#sku-entry-empty').clone(true, true).addClass('sku-entry').appendTo('#sku-entries').show();
}

// Returns false if any required-class input fields within the provded
// selector is empty
function CheckRequiredEntries(selector)
{
	var requiredFields = selector.find('.required');
	requiredFields.parent().removeClass('requires-value');
	var unfilled = requiredFields.filter(function() { return $(this).val() == ""; });
	unfilled.parent().addClass('requires-value');
	return unfilled.length == 0;
}

// Returns false if there are no SKUs added
function SkusAdded()
{
	if ($('.sku-entry').length)
	{
		return true;
	}
	else
	{
		alert("No SKUs added");
		return false;
	}
}


// Prints for configured label sheet
function Print(skuCount)
{
	// Clear any previous prints
	$('.sku-label-entry').remove();
	
	// Load SKUs into cells
	$('.sku-entry').each(function() {
		for (var i = 0; i < skuCount; i++)
		{
			var newCell = $('#sku-cell-empty').clone().addClass('sku-label-entry').appendTo('#sku-label-sheet').show();
			var sku = $(this).find('.sku').val();
			var item = $(this).find('.item').val();
			var desc = $(this).find('.desc').val();
			var price = $(this).find('.price').val();
			
			newCell.find('.barcode').JsBarcode(sku, {
				format: "CODE128",
				width: 1,
				height: 25,
				textMargin: 2,
				fontSize: 8,
				margin: 1
			});
			newCell.find('.item').html(item);
			if (desc)
			{
				newCell.find('.desc').html(desc);
			}
			else
			{
				newCell.find('.desc').hide();
			}
			newCell.find('.price').html(price);
		}
	});
	
	// Print
	$('#sku-label-sheet').show().print().hide();
	
	// Hide the overlay
	HideOverlay();
}


// Deletes all added SKUs
function Clear()
{
	$('.sku-entry').remove();
}


// Converts all entries into a CSV text thing
function Save()
{
	var csv = '';
	$('.sku-entry').each(function(){
		var sku = $(this).find('.sku').val();
		var item = $(this).find('.item').val();
		var desc = $(this).find('.desc').val();
		var price = $(this).find('.price').val();
		csv += sku + "," + item + "," + desc + "," + price + "\n";
	});
	download(csv, "sku-generator.csv", "text/css");
}

// Function to download data to a file
// https://stackoverflow.com/questions/13405129/javascript-create-and-save-file
function download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
}


// Loads the provided string data
function LoadData(data)
{
	// Validate input data:
	// Split the input data on newlines, then commas.
	// If the number of fields in each entry is not correct, fail.
	var entries = data.split("\n");
	if (entries.length == 0)
	{
		return false;
	}
	for (i in entries)
	{
		if ((entries[i]) && (entries[i][0] != '#') && (entries[i].split(",").length != 4))
		{
			return false;
		}
	}
	
	// File looks ok, load up the values
	
	// Clear existing stuff
	Clear();
	
	// Add new stuff
	for (i in entries)
	{
		// skip empty lines and comments
		if ((! entries[i]) || (entries[i][0] == '#'))
		{
			continue;
		}
		var entry = entries[i].split(',');
		var skuEntry = AddSkuEntry();
		skuEntry.find('.sku').val(entry[0]);
		skuEntry.find('.item').val(entry[1]);
		skuEntry.find('.desc').val(entry[2]);
		skuEntry.find('.price').val(entry[3]);
	}
	
	// Hide the overlay
	HideOverlay();
	
	return true;
}
